"use strict";
/** @format */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeUserRouter = void 0;
const fs_1 = __importDefault(require("fs"));
const passport_1 = __importDefault(require("passport"));
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const express_validator_1 = require("express-validator");
const middlewares_1 = require("../loaders/middlewares");
const validations_1 = require("../middleware/validations");
const makeUserRouter = (context) => {
    const router = express_1.default.Router();
    // Define routes
    const { cacheService, authService } = context.services;
    router.post('/', passport_1.default.authenticate('jwt', { session: false }), (0, middlewares_1.roles)(['admin']), middlewares_1.upload.single('image'), validations_1.validateRegister, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (req.validationErrors)
                throw new Error('Validation errors');
            if (!req.file) {
                return res.status(400).send('No image uploaded');
            }
            const user = yield authService.register(Object.assign(Object.assign({}, req.body), { image: req.file.filename }));
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = user.dataValues, { password } = _a, rest = __rest(_a, ["password"]);
            res.status(201);
            res.json(rest);
        }
        catch (error) {
            if (req.file)
                fs_1.default.unlinkSync(req.file.path);
            if (req.validationErrors)
                return res.status(400).send('Fields are not valid');
            next(error.message);
        }
    }));
    router.get('/', passport_1.default.authenticate('jwt', { session: false }), (0, middlewares_1.roles)(['admin']), validations_1.validatePagination, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { pageSize, page } = req.query;
            const users = yield authService.getAll(pageSize, page);
            res.status(200).header('X-total-count', users.length).json(users);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.get('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? req.params.id : req.user.id;
            const user = yield authService.findById(userId);
            if (!user) {
                return res.status(404).send('The user with the provided ID does not exist.');
            }
            res.status(200);
            res.json(user);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.get('/:id/cv', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const id = req.user.role === 'admin' ? req.params.id : req.user.id;
            const cachedCV = yield cacheService.get(`user-${id}:cv`);
            if (cachedCV)
                return res.json(JSON.parse(cachedCV));
            const cv = yield authService.CV(id);
            if (!cv) {
                return res.status(404).send('The user with the provided ID does not exist.');
            }
            yield cacheService.set(`user-${id}:cv`, JSON.stringify(cv), 60000);
            res.status(200);
            res.json(cv);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.put('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? req.params.id : req.user.id;
            let userBody = Object.assign({}, req.body);
            if (req.user.role !== 'admin') {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const _b = req.body, { image, password, role } = _b, rest = __rest(_b, ["image", "password", "role"]);
                userBody = rest;
            }
            yield authService.update(userId, userBody);
            const user = yield authService.findById(userId);
            if (!user) {
                return res.status(404).send('The user with the provided ID does not exist.');
            }
            res.status(200);
            res.json(user);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.delete('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? req.params.id : req.user.id;
            const { image } = yield authService.findById(userId);
            const filePath = path_1.default.resolve(__dirname, `../../public/${image}`);
            const deletedUser = yield authService.deleteUser(userId);
            if (!deletedUser) {
                return res.status(404).send('The user with the provided ID does not exist.');
            }
            if (fs_1.default.existsSync(filePath)) {
                fs_1.default.unlinkSync(filePath);
            }
            res.status(202).send('Deleted user account.');
        }
        catch (error) {
            next(error.message);
        }
    }));
    return router;
};
exports.makeUserRouter = makeUserRouter;
//# sourceMappingURL=users.js.map