// /* eslint-disable no-undef */
// import * as dotenv from 'dotenv';
// import supertest from 'supertest';
// import { loadApp } from '../loaders/app'; // Import your Express app
// dotenv.config();
// describe('Authentication and Authorization', () => {
//   let request;
//   let app;
//   let adminToken;
//   let userToken;
//   let guestToken;
//   beforeAll(async () => {
//     // Authenticate users with different roles and store their tokens
//     app = await loadApp();
//     request = supertest(app);
//     const userAuthResponse = await request
//       .post('/auth')
//       .send({ email: 'user', password: 'userpassword' });
//     userToken = userAuthResponse.body.token;
//     console.log(userAuthResponse, ' here we aree');
//     const guestAuthResponse = await request
//       .post('/auth')
//       .send({ username: 'guest', password: 'guestpassword' });
//     guestToken = guestAuthResponse.body.token;
//   });
//   it('should allow admin access to admin-only endpoint', async (done) => {
//     const response = await request.get('/admin-only').set('Authorization', `Bearer ${adminToken}`);
//     expect(response.status).toBe(200);
//     console.log('we are heree');
//     done();
//   });
//   // it('should allow user access to user-only endpoint', async (done) => {
//   //   const response = await request.get('/user-only').set('Authorization', `Bearer ${userToken}`);
//   //   expect(response.status).toBe(200);
//   //   done();
//   // });
//   // it('should allow guest access to guest-only endpoint', async (done) => {
//   //   const response = await request.get('/guest-only').set('Authorization', `Bearer ${guestToken}`);
//   //   expect(response.status).toBe(200);
//   //   done();
//   // });
//   // it('should deny guest access to admin-only endpoint', async (done) => {
//   //   const response = await request.get('/admin-only').set('Authorization', `Bearer ${guestToken}`);
//   //   expect(response.status).toBe(403); // Adjust the status code based on your authorization logic
//   //   done();
//   // });
//   // Additional test cases for other combinations and roles
// });
//# sourceMappingURL=auth.test.js.map