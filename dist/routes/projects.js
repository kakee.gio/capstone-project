"use strict";
/** @format */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeProjectsRouter = void 0;
const express_1 = __importDefault(require("express"));
const express_validator_1 = require("express-validator");
const passport_1 = __importDefault(require("passport"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const middlewares_1 = require("../loaders/middlewares");
const validations_1 = require("../middleware/validations");
const makeProjectsRouter = (context) => {
    const router = express_1.default.Router();
    const { projectService, cacheService } = context.services;
    // Define routes
    router.post('/', passport_1.default.authenticate('jwt', { session: false }), middlewares_1.upload.single('image'), validations_1.validateProject, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (req.validationErrors)
                throw new Error('Validation errors');
            if (!req.file) {
                return res.status(400).send('Invalid file. Only JPEG, PNG, and GIF images are allowed.');
            }
            const projectToAdd = Object.assign(Object.assign({}, req.body), { image: req.file.filename });
            if (req.user.role !== 'admin') {
                projectToAdd.userId = req.user.id;
            }
            const project = yield projectService.add(projectToAdd);
            res.status(201);
            res.json(project);
        }
        catch (error) {
            if (req.file)
                fs_1.default.unlinkSync(req.file.path);
            if (req.validationErrors)
                return res.status(400).send('Fields are not valid');
            next(error.message);
        }
    }));
    router.get('/', passport_1.default.authenticate('jwt', { session: false }), (0, middlewares_1.roles)(['admin']), validations_1.validatePagination, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { pageSize, page } = req.query;
            const projects = yield projectService.getAll(pageSize, page);
            res.status(200).header('X-total-count', projects.length).json(projects);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.get('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? null : req.user.id;
            const project = yield projectService.findById(req.params.id, userId);
            if (!project) {
                return res.status(404).send('The Project with the provided ID does not exist.');
            }
            res.status(200);
            res.json(project);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.put('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? null : req.user.id;
            const body = Object.assign({}, req.body);
            if (userId)
                body.userId = userId;
            const updatedProject = yield projectService.update(req.params.id, body, userId);
            if (!updatedProject[0]) {
                return res.status(404).send('Not Found');
            }
            const project = yield projectService.findById(req.params.id, userId);
            yield cacheService.del(`user-${userId || req.params.id}:cv`);
            res.status(200);
            res.json(project);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.delete('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? null : req.user.id;
            const project = yield projectService.findById(req.params.id, userId);
            const filePath = path_1.default.resolve(__dirname, `../../public/${project.image}`);
            const deletedProject = yield projectService.deleteProject(req.params.id, userId);
            if (!deletedProject) {
                return res.status(404).send('Not Found');
            }
            if (fs_1.default.existsSync(filePath)) {
                fs_1.default.unlinkSync(filePath);
            }
            res.status(204).send('Deleted a project');
        }
        catch (error) {
            next(error.message);
        }
    }));
    return router;
};
exports.makeProjectsRouter = makeProjectsRouter;
//# sourceMappingURL=projects.js.map