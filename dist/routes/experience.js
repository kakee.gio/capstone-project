"use strict";
/** @format */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeExperienceRouter = void 0;
const passport_1 = __importDefault(require("passport"));
const express_1 = __importDefault(require("express"));
const express_validator_1 = require("express-validator");
const middlewares_1 = require("../loaders/middlewares");
const validations_1 = require("../middleware/validations");
const makeExperienceRouter = (context) => {
    const router = express_1.default.Router();
    const { experienceService, cacheService } = context.services;
    router.post('/', passport_1.default.authenticate('jwt', { session: false }), validations_1.validateExperience, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (req.validationErrors)
                throw new Error('Validation errors');
            let experience;
            if (req.user.role === 'admin') {
                experience = yield experienceService.add(req.body);
            }
            else {
                experience = yield experienceService.add(Object.assign(Object.assign({}, req.body), { user_id: req.user.id }));
            }
            res.status(201);
            res.json(experience);
        }
        catch (error) {
            if (req.validationErrors)
                return res.status(400).send('Fields are not valid');
            next(error.message);
        }
    }));
    router.get('/', passport_1.default.authenticate('jwt', { session: false }), (0, middlewares_1.roles)(['admin']), validations_1.validatePagination, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { pageSize, page } = req.query;
            const experiences = yield experienceService.getAll(pageSize, page);
            res.status(200).header('X-total-count', experiences.length).json(experiences);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.get('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? null : req.user.id;
            const experience = yield experienceService.findById(req.params.id, userId);
            if (!experience) {
                return res.status(404).send('Experience with the provided ID does not exist.');
            }
            res.json(experience);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.put('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? null : req.user.id;
            const body = Object.assign({}, req.body);
            if (userId) {
                body.userId = userId;
            }
            const updatedExperience = yield experienceService.update(req.params.id, body, userId);
            if (!updatedExperience[0]) {
                return res.status(404).send('Experience with the provided ID does not exist.');
            }
            yield cacheService.del(`user-${userId || req.params.id}:cv`);
            const experience = yield experienceService.findById(req.params.id);
            res.json(experience);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.delete('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? null : req.user.id;
            const deletedExperience = yield experienceService.deleteExperience(req.params.id, userId);
            if (!deletedExperience) {
                return res.status(404).send('Experience not found.');
            }
            res.status(204).send('Deleted experience.');
        }
        catch (error) {
            next(error.message);
        }
    }));
    return router;
};
exports.makeExperienceRouter = makeExperienceRouter;
//# sourceMappingURL=experience.js.map