"use strict";
/** @format */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeAuthRouter = void 0;
const express_1 = __importDefault(require("express"));
const fs_1 = __importDefault(require("fs"));
const middlewares_1 = require("../loaders/middlewares");
const validations_1 = require("../middleware/validations");
const makeAuthRouter = (context) => {
    const router = express_1.default.Router();
    const { authService } = context.services;
    router.post('/register', middlewares_1.upload.single('image'), validations_1.validateRegister, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (req.validationErrors)
                throw new Error('Validation errors');
            if (!req.file) {
                return res.status(400).send('Invalid file. Only JPEG, PNG, and GIF images are allowed.');
            }
            const user = yield authService.register(Object.assign(Object.assign({}, req.body), { image: req.file.filename, role: 'user' }));
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = user.dataValues, { password } = _a, rest = __rest(_a, ["password"]);
            res.status(201);
            res.json(rest);
        }
        catch (error) {
            if (req.file)
                fs_1.default.unlinkSync(req.file.path);
            if (req.validationErrors)
                return res.status(400).send('Fields are not valid');
            next(error.message);
        }
    }));
    router.post('/login', validations_1.validateLogin, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (req.validationErrors)
                throw new Error('Validation errors');
            const userNToken = yield authService.login(req.body.email, req.body.password);
            res.status(200);
            res.json(userNToken);
        }
        catch (error) {
            if (req.validationErrors)
                return res.status(400).send('Fields are not valid');
            next(error);
        }
    }));
    return router;
};
exports.makeAuthRouter = makeAuthRouter;
//# sourceMappingURL=auth.js.map