/** @format */
Object.defineProperty(exports, '__esModule', { value: true });
exports.registerRouter = void 0;
const express_1 = require('express');

exports.registerRouter = (0, express_1.Router)();
exports.registerRouter.get('/', (req, res) => {
  res.send('olaa am register route');
});
exports.registerRouter.post('/', (req, res) => {
  console.log(req.body, ' this should be a body');
  res.json(req.body);
});
// # sourceMappingURL=register.js.map
