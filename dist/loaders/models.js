"use strict";
/** @format */
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadModels = void 0;
const user_model_1 = require("../models/user.model");
const project_model_1 = require("../models/project.model");
const experience_model_1 = require("../models/experience.model");
const feedback_model_1 = require("../models/feedback.model");
const loadModels = (sequelize) => {
    const models = {
        user: user_model_1.User,
        project: project_model_1.Project,
        experience: experience_model_1.Experience,
        feedback: feedback_model_1.Feedback,
    };
    // for (const model of Object.values(models)) {
    // 	model.defineSchema(sequelize);
    // }
    Object.values(models).forEach((model) => {
        model.defineSchema(sequelize);
    });
    Object.values(models).forEach((model) => {
        model.associate(models, sequelize);
    });
    // for (const model of Object.values(models)) {
    // 	model.associate(models, sequelize);
    // }
    return models;
};
exports.loadModels = loadModels;
//# sourceMappingURL=models.js.map