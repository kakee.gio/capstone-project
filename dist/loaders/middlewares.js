"use strict";
/** @format */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.upload = exports.loadMiddlewares = exports.validateEmail = exports.roles = void 0;
const body_parser_1 = __importDefault(require("body-parser"));
const express_validator_1 = require("express-validator");
const multer_1 = __importDefault(require("multer"));
const path = __importStar(require("path"));
const roles = (allowedRoles) => (req, res, next) => {
    if (allowedRoles.includes(req.user.role)) {
        next();
    }
    else {
        res.status(403).send('Forbidden');
    }
};
exports.roles = roles;
const validateEmail = (req) => new Promise((resolve, reject) => {
    (0, express_validator_1.body)('email').isEmail().withMessage('Invalid email address')(req, null, () => {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            reject(errors.array());
        }
        else {
            resolve();
        }
    });
});
exports.validateEmail = validateEmail;
const loadMiddlewares = (app) => {
    app.use(body_parser_1.default.json());
};
exports.loadMiddlewares = loadMiddlewares;
const publicPath = path.resolve(__dirname, '../../public');
const storage = multer_1.default.diskStorage({
    destination: (req, file, cb) => {
        cb(null, publicPath);
    },
    filename: (req, file, cb) => {
        const timestamp = Date.now();
        const filename = `${timestamp}-${file.originalname}`;
        cb(null, filename);
    },
});
const imageFileFilter = (req, file, cb) => {
    const allowedMimeTypes = ['image/jpeg', 'image/png', 'image/gif'];
    if (allowedMimeTypes.includes(file.mimetype)) {
        cb(null, true);
    }
    else {
        cb(null, false);
    }
};
exports.upload = (0, multer_1.default)({ storage, fileFilter: imageFileFilter });
//# sourceMappingURL=middlewares.js.map