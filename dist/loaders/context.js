"use strict";
/** @format */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadContext = void 0;
const auth_service_1 = __importDefault(require("../services/auth.service"));
const experience_service_1 = require("../services/experience.service");
const project_service_1 = require("../services/project.service");
const feedback_service_1 = require("../services/feedback.service");
const cache_service_1 = require("../services/cache.service");
const loadContext = (sequelize, redisConfig) => __awaiter(void 0, void 0, void 0, function* () {
    return ({
        services: {
            authService: new auth_service_1.default(sequelize),
            experienceService: new experience_service_1.ExperienceService(sequelize),
            projectService: new project_service_1.ProjectService(sequelize),
            feedbackService: new feedback_service_1.FeedbackService(sequelize),
            cacheService: new cache_service_1.CacheService(redisConfig),
        },
    });
});
exports.loadContext = loadContext;
//# sourceMappingURL=context.js.map