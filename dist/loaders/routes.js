"use strict";
/** @format */
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadRoutes = void 0;
const auth_1 = require("../routes/auth");
const users_1 = require("../routes/users");
const experience_1 = require("../routes/experience");
const feedback_1 = require("../routes/feedback");
const projects_1 = require("../routes/projects");
const loadRoutes = (app, context) => {
    app.use('/api/auth', (0, auth_1.makeAuthRouter)(context));
    app.use('/api/user', (0, users_1.makeUserRouter)(context));
    app.use('/api/experience', (0, experience_1.makeExperienceRouter)(context));
    app.use('/api/feedback', (0, feedback_1.makeFeedbackRouter)(context));
    app.use('/api/project', (0, projects_1.makeProjectsRouter)(context));
};
exports.loadRoutes = loadRoutes;
//# sourceMappingURL=routes.js.map