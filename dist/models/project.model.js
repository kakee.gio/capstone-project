"use strict";
/** @format */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Project = void 0;
const sequelize_1 = require("sequelize");
class Project extends sequelize_1.Model {
    static defineSchema(sequelize) {
        Project.init({
            id: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true,
            },
            userId: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                field: 'user_id',
                references: {
                    model: 'users',
                    key: 'id',
                },
            },
            image: {
                type: sequelize_1.DataTypes.STRING(256),
                allowNull: false,
            },
            description: {
                type: sequelize_1.DataTypes.STRING(256),
                allowNull: false,
            },
        }, {
            tableName: 'projects',
            underscored: true,
            sequelize,
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            timestamps: true,
        });
    }
    static associate(models) {
        Project.belongsTo(models.user, { foreignKey: 'user_id' });
    }
}
exports.Project = Project;
//# sourceMappingURL=project.model.js.map