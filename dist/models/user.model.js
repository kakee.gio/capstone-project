"use strict";
/** @format */
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = exports.UserRole = void 0;
const sequelize_1 = require("sequelize");
const experience_model_1 = require("./experience.model");
// eslint-disable-next-line no-shadow
var UserRole;
(function (UserRole) {
    UserRole["Admin"] = "admin";
    UserRole["User"] = "user";
})(UserRole = exports.UserRole || (exports.UserRole = {}));
class User extends sequelize_1.Model {
    static defineSchema(sequelize) {
        User.init({
            id: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true,
            },
            firstName: {
                field: 'first_name',
                type: new sequelize_1.DataTypes.STRING(128),
                allowNull: false,
                validate: {
                    len: [3, 30],
                },
            },
            lastName: {
                field: 'last_name',
                type: new sequelize_1.DataTypes.STRING(128),
                allowNull: false,
                validate: {
                    len: [3, 30],
                },
            },
            image: {
                type: new sequelize_1.DataTypes.STRING(256),
                allowNull: false,
            },
            title: {
                type: new sequelize_1.DataTypes.STRING(256),
                allowNull: false,
            },
            summary: {
                type: new sequelize_1.DataTypes.STRING(256),
                allowNull: false,
            },
            role: {
                type: new sequelize_1.DataTypes.STRING(50),
                allowNull: false,
            },
            email: {
                type: sequelize_1.DataTypes.STRING,
                allowNull: false,
                unique: true,
                validate: {
                    isEmail: true,
                },
            },
            password: {
                type: sequelize_1.DataTypes.STRING,
                allowNull: false,
            },
        }, {
            tableName: 'users',
            underscored: true,
            sequelize,
        });
    }
    static associate(models) {
        User.hasMany(models.experience);
        experience_model_1.Experience.belongsTo(models.user, {
            foreignKey: {
                allowNull: false,
            },
            onDelete: 'CASCADE',
        });
        User.hasMany(models.project, {
            foreignKey: 'user_id',
            as: 'projects',
            onDelete: 'CASCADE',
        });
        User.hasMany(models.feedback, {
            foreignKey: 'from_user',
            as: 'feedbacksFromUser',
            onDelete: 'CASCADE',
        });
        User.hasMany(models.feedback, {
            foreignKey: 'to_user',
            as: 'feedbacksToUser',
            onDelete: 'CASCADE',
        });
    }
}
exports.User = User;
//# sourceMappingURL=user.model.js.map