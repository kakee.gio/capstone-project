"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CacheService = void 0;
const redis = __importStar(require("redis"));
const util_1 = require("util");
class CacheService {
    constructor(redisConfig) {
        this.client = redis.createClient(redisConfig);
        this.client.on('error', (error) => {
            console.error('Redis error:', error);
            process.exit(1);
        });
        this.client.on('connect', () => {
            console.log('\nConnected to Redis');
        });
    }
    asyncGet(key) {
        return __awaiter(this, void 0, void 0, function* () {
            const getAsync = (0, util_1.promisify)(this.client.get).bind(this.client);
            return getAsync(key);
        });
    }
    asyncSet(key, value, expirationInSeconds) {
        return __awaiter(this, void 0, void 0, function* () {
            const setAsync = (0, util_1.promisify)(this.client.set).bind(this.client);
            yield setAsync(key, value, 'EX', expirationInSeconds);
        });
    }
    asyncDel(key) {
        return __awaiter(this, void 0, void 0, function* () {
            const delAsync = (0, util_1.promisify)(this.client.del).bind(this.client);
            yield delAsync(key);
        });
    }
    set(key, value, expirationInSeconds) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.asyncSet(key, JSON.stringify(value), expirationInSeconds);
                return true;
            }
            catch (error) {
                console.error('Error setting cache:', error);
                return false;
            }
        });
    }
    get(key) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cachedData = yield this.asyncGet(key);
                return cachedData ? JSON.parse(cachedData) : null;
            }
            catch (error) {
                console.error('Error getting cache:', error);
                return null;
            }
        });
    }
    del(key) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.asyncDel(key);
                return true;
            }
            catch (error) {
                console.error('Error deleting cache:', error);
                return false;
            }
        });
    }
}
exports.CacheService = CacheService;
//# sourceMappingURL=cache.service.js.map