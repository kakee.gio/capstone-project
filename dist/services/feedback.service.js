"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FeedbackService = void 0;
class FeedbackService {
    constructor(sequelize) {
        this.client = sequelize;
        this.models = this.client.models;
    }
    getAll(limit, offset) {
        return __awaiter(this, void 0, void 0, function* () {
            const attributes = ['id', 'from_user', 'to_user', 'company_name', 'content'];
            const options = {
                attributes,
            };
            if (limit)
                options.limit = parseInt(limit, 10);
            if (offset)
                options.offset = parseInt(offset, 10) - 1;
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const allFeedback = yield this.models.Feedback.findAll(options);
                    resolve(allFeedback);
                }
                catch (error) {
                    reject(Error);
                }
            }));
        });
    }
    add(Feedback) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const newFeedback = yield this.models.Feedback.create(Feedback);
                    resolve(newFeedback);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    findById(feedback_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const where = {
                        id: feedback_id,
                    };
                    if (user_id)
                        where.from_user = user_id;
                    const feedback = yield this.models.Feedback.findOne({
                        where,
                    });
                    resolve(feedback);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    update(feedback_id, data, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const where = {
                        id: feedback_id,
                    };
                    if (user_id)
                        where.from_user = user_id;
                    const updatedFeedback = yield this.models.Feedback.update(data, {
                        where,
                    });
                    resolve(updatedFeedback);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    deleteFeedback(feedback_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const where = {
                        id: feedback_id,
                    };
                    if (user_id)
                        where.from_user = user_id;
                    const deletedFeedback = yield this.models.Feedback.destroy({ where });
                    resolve(deletedFeedback);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
}
exports.FeedbackService = FeedbackService;
//# sourceMappingURL=feedback.service.js.map