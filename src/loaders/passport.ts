/** @format */

import * as dotenv from 'dotenv';
import passport from 'passport';
import passportJWT, { ExtractJwt } from 'passport-jwt';
import { Loader } from '../interfaces/general';

dotenv.config();

// const ExtraJWT = passportJWT.ExtractJwt;
const JWTStrategy = passportJWT.Strategy;

export const loadPassport: Loader = (app) => {
  const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.SECRET_TOKEN,
  };

  const strategy = new JWTStrategy(jwtOptions, (jwtPayload, done) => {
    done(null, jwtPayload);
  });

  passport.use(strategy);
  app.use(passport.initialize());
};
