/** @format */

import express from 'express';
import expressRequestId from 'express-request-id';
import { loadMiddlewares } from './middlewares';
import { loadRoutes } from './routes';
import { loadContext } from './context';
import { loadModels } from './models';
import { loadSequelize } from './sequelize';
import { config } from '../config';
import { loadPassport } from './passport';
import { setCorrelationId, errorHandler } from '../libs/logger';

export const loadApp = async () => {
  try {
    const app = express();

    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    app.use(expressRequestId());
    app.use(setCorrelationId);

    const sequelize = loadSequelize(config);

    await sequelize.authenticate();

    const context = await loadContext(sequelize, config.redis);
    process.stdout.write('\nConnected to Sequelize\n');

    loadModels(sequelize);

    loadPassport(app, context);

    loadMiddlewares(app, context);

    loadRoutes(app, context);

    app.use(errorHandler);

    app.use((req, res) => {
      res.status(404).json({ error: 'Not Found' });
    });

    return app;
  } catch (error) {
    process.exit(1);
  }
};
