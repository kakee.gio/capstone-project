/** @format */

import { Sequelize } from 'sequelize';
import { Context } from '../interfaces/general';
import AuthService from '../services/auth.service';
import { ExperienceService } from '../services/experience.service';
import { ProjectService } from '../services/project.service';
import { FeedbackService } from '../services/feedback.service';
import { CacheService } from '../services/cache.service';

export const loadContext = async (sequelize: Sequelize, redisConfig: any): Promise<Context> => ({
  services: {
    authService: new AuthService(sequelize),
    experienceService: new ExperienceService(sequelize),
    projectService: new ProjectService(sequelize),
    feedbackService: new FeedbackService(sequelize),
    cacheService: new CacheService(redisConfig),
  },
});
