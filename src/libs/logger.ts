/** @format */
import * as dotenv from 'dotenv';
import pino from 'pino';
import { v4 } from 'uuid';

dotenv.config();

export const logger = pino({
  prettyPrint: process.env.NODE_ENV === 'development',
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const errorHandler = (err, req, res, _next: any) => {
  logger.error({ requestId: req.id, error: err }, 'Error occurred in the request.');

  res.status(505).send('Something went wrong on the server');
};

export const setCorrelationId = (req: any, res: any, next: any) => {
  req.id = req.id || v4();
  req.logger = logger;
  logger.info({ requestId: req.requestId }, `Request received: ${req.method} ${req.url}`);

  next();
};
