/** @format */
import { Sequelize } from 'sequelize';

export class FeedbackService {
  private client: Sequelize;

  private models;

  constructor(sequelize: Sequelize) {
    this.client = sequelize;
    this.models = this.client.models;
  }

  async getAll(limit, offset) {
    const attributes = ['id', 'from_user', 'to_user', 'company_name', 'content'];

    const options: any = {
      attributes,
    };

    if (limit) options.limit = parseInt(limit, 10);

    if (offset) options.offset = parseInt(offset, 10) - 1;

    return new Promise(async (resolve, reject) => {
      try {
        const allFeedback = await this.models.Feedback.findAll(options);

        resolve(allFeedback);
      } catch (error) {
        reject(Error);
      }
    });
  }

  async add(Feedback: any) {
    return new Promise(async (resolve, reject) => {
      try {
        const newFeedback = await this.models.Feedback.create(Feedback);
        resolve(newFeedback);
      } catch (error) {
        reject(error);
      }
    });
  }

  async findById(feedback_id: number, user_id?: number) {
    return new Promise(async (resolve, reject) => {
      try {
        const where: any = {
          id: feedback_id,
        };

        if (user_id) where.from_user = user_id;

        const feedback = await this.models.Feedback.findOne({
          where,
        });

        resolve(feedback);
      } catch (error) {
        reject(error);
      }
    });
  }

  async update(feedback_id: number, data: any, user_id?: number) {
    return new Promise(async (resolve, reject) => {
      try {
        const where: any = {
          id: feedback_id,
        };

        if (user_id) where.from_user = user_id;

        const updatedFeedback = await this.models.Feedback.update(data, {
          where,
        });

        resolve(updatedFeedback);
      } catch (error) {
        reject(error);
      }
    });
  }

  async deleteFeedback(feedback_id: number, user_id?: number) {
    return new Promise(async (resolve, reject) => {
      try {
        const where: any = {
          id: feedback_id,
        };

        if (user_id) where.from_user = user_id;

        const deletedFeedback = await this.models.Feedback.destroy({ where });

        resolve(deletedFeedback);
      } catch (error) {
        reject(error);
      }
    });
  }
}
