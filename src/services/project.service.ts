/** @format */

import { Sequelize } from 'sequelize';

export class ProjectService {
  private client: Sequelize;

  private models;

  constructor(sequelize: Sequelize) {
    this.client = sequelize;
    this.models = this.client.models;
  }

  async getAll(limit, offset) {
    const attributes = ['id', 'user_id', 'image', 'description', 'created_at'];

    const options: any = {
      attributes,
    };

    if (limit) options.limit = parseInt(limit, 10);

    if (offset) options.offset = parseInt(offset, 10) - 1;

    return new Promise(async (resolve, reject) => {
      try {
        const allProject = await this.models.Project.findAll(options);

        resolve(allProject);
      } catch (error) {
        reject(error);
      }
    });
  }

  async add(Project: any) {
    return new Promise(async (resolve, reject) => {
      try {
        const newProject = await this.models.Project.create(Project);
        resolve(newProject);
      } catch (error) {
        reject(error);
      }
    });
  }

  async findById(projectId: number, userId?: number) {
    return new Promise(async (resolve, reject) => {
      try {
        const where: any = {
          id: projectId,
        };

        if (userId) where.userId = userId;

        const project = await this.models.Project.findOne({
          where,
        });

        resolve(project);
      } catch (error) {
        reject(error);
      }
    });
  }

  async update(project_id: number, data: any, user_id?: number) {
    return new Promise(async (resolve, reject) => {
      try {
        const where: any = {
          id: project_id,
        };

        if (user_id) where.user_id = user_id;

        const updatedProject = await this.models.Project.update(data, { where });

        resolve(updatedProject);
      } catch (error) {
        reject(error);
      }
    });
  }

  async deleteProject(project_id: number, user_id?: number) {
    return new Promise(async (resolve, reject) => {
      try {
        const where: any = {
          id: project_id,
        };

        if (user_id) where.user_id = user_id;

        const deletedProject = await this.models.Project.destroy({ where });

        resolve(deletedProject);
      } catch (error) {
        reject(error);
      }
    });
  }
}
