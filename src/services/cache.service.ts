import * as redis from 'redis';
import { promisify } from 'util';

export class CacheService {
  private client: redis.RedisClient;

  constructor(redisConfig: redis.ClientOpts) {
    this.client = redis.createClient(redisConfig);

    this.client.on('error', (error) => {
      console.error('Redis error:', error);
      process.exit(1);
    });
    this.client.on('connect', () => {
      console.log('\nConnected to Redis');
    });
  }

  private async asyncGet(key: string): Promise<string | null> {
    const getAsync = promisify(this.client.get).bind(this.client);
    return getAsync(key);
  }

  private async asyncSet(key: string, value: string, expirationInSeconds: number): Promise<void> {
    const setAsync = promisify(this.client.set).bind(this.client);
    await setAsync(key, value, 'EX', expirationInSeconds);
  }

  private async asyncDel(key: string): Promise<void> {
    const delAsync = promisify(this.client.del).bind(this.client);
    await delAsync(key);
  }

  async set(key: string, value: any, expirationInSeconds: number): Promise<boolean> {
    try {
      await this.asyncSet(key, JSON.stringify(value), expirationInSeconds);

      return true;
    } catch (error) {
      console.error('Error setting cache:', error);
      return false;
    }
  }

  async get<T>(key: string): Promise<T | null> {
    try {
      const cachedData = await this.asyncGet(key);
      return cachedData ? JSON.parse(cachedData) : null;
    } catch (error) {
      console.error('Error getting cache:', error);
      return null;
    }
  }

  async del(key: string): Promise<boolean> {
    try {
      await this.asyncDel(key);
      return true;
    } catch (error) {
      console.error('Error deleting cache:', error);
      return false;
    }
  }
}
