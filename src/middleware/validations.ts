import { body, query, validationResult } from 'express-validator';

export const validateRegister = [
  body('email').isEmail().withMessage('Invalid email format'),
  body('firstName').isLength({ min: 3 }).withMessage('length should be more then 3'),
  body('lastName').isLength({ min: 3 }).withMessage('length should be more then 3'),
  body('summary').isString().isLength({ min: 3 }).withMessage('length should be more then 3'),
  body('title').isString().isLength({ min: 3 }).withMessage('length should be more then 3'),
  body('password').isString().isLength({ min: 3 }).withMessage('length should be more then 3'),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.validationErrors = errors.array();
    }
    next();
  },
];

export const validatePagination = [
  query('pageSize').optional().isInt().withMessage('pageSize must be a number'),
  query('page').optional().isInt().withMessage('page must be a number'),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.validationErrors = errors.array();
    }
    next();
  },
];

export const validateLogin = [
  body('email').isEmail().withMessage('Invalid email format'),
  body('password').isString().isLength({ min: 3 }).withMessage('length should be more then 3'),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.validationErrors = errors.array();
    }
    next();
  },
];

export const validateExperience = [
  body('companyName').isString().isLength({ min: 3 }).withMessage('Length should be more than 3'),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.validationErrors = errors.array();
    }
    next();
  },
];

export const validateFeedback = [
  body('fromUser').isInt(),
  body('toUser').isInt(),
  body('content').isString(),
  body('companyName').isString().isLength({ min: 3 }).withMessage('Length should be more than 3'),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.validationErrors = errors.array();
    }
    next();
  },
];

export const validateProject = [
  body('description').isString(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.validationErrors = errors.array();
    }
    next();
  },
];
