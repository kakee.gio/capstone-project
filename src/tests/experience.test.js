/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-undef */
const supertest = require('supertest');
const { loadApp } = require('../loaders/app');

describe('API Tests', () => {
  let app;
  let bearerToken;
  const email = 'zimbabue@gmail.com';
  const password = 'zimbabue';
  let userId;
  let experienceId;

  beforeAll(async () => {
    app = await loadApp();
  });

  it('should return a 200 OK response and log in an admin user with valid email and password', async () => {
    const response = await supertest(app).post('/api/auth/login').send({ email, password });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('token', expect.any(String));

    expect(response.body).toHaveProperty('user');
    const { user } = response.body;
    userId = user.id;

    bearerToken = `Bearer ${response.body.token}`;

    expect(bearerToken).toBeDefined();

    expect(user).toHaveProperty('id', expect.any(Number));
    expect(user).toHaveProperty('email', email);
    expect(user).toHaveProperty('role', 'admin');
  });

  it('should return all experience', async () => {
    const response = await supertest(app).get('/api/experience').set('Authorization', bearerToken);

    expect(response.status).toBe(200);
  });

  it('should create new experience', async () => {
    const response = await supertest(app)
      .post('/api/experience')
      .set('Authorization', bearerToken)
      .send({
        userId: 1,
        companyName: 'Example company name',
        role: 'admin',
        startDate: '2023-12-11',
        endDate: '2023-11-11',
      });

    const experience = response.body;

    experienceId = experience.id;

    expect(experience).toHaveProperty('id', expect.any(Number));
    expect(experience).toHaveProperty('userId', 1);
    expect(response.status).toBe(201);
  });

  it('should update an experience', async () => {
    const newCompanyName = 'experience is updated';

    const response = await supertest(app)
      .put(`/api/experience/${experienceId}`)
      .set('Authorization', bearerToken)
      .send({
        companyName: newCompanyName,
      });

    const experience = response.body;

    expect(experience).toHaveProperty('companyName', newCompanyName);
    expect(experience).toHaveProperty('userId', 1);
    expect(response.status).toBe(200);
  });

  it('should delete experience', async () => {
    const response = await supertest(app)
      .delete(`/api/experience/${experienceId}`)
      .set('Authorization', bearerToken);

    expect(response.status).toBe(204);
  });
});
