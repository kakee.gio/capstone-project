/* eslint-disable no-undef */
const path = require('path');
const supertest = require('supertest');
const { loadApp } = require('../loaders/app');

describe('API Tests', () => {
  let app;
  let bearerToken;
  const email = 'josshn@email.com';
  const password = 'zimbabue';
  let userId;

  beforeAll(async () => {
    app = await loadApp();
  });

  it('should register a user with file upload', async () => {
    const filePath = `${path.resolve(__dirname, '../../public/default.png')}`;

    const response = await supertest(app)
      .post('/api/auth/register')
      .field('firstName', 'John Doe')
      .field('lastName', 'last name ')
      .field('summary', 'John Doe summary')
      .field('title', 'John Doe title')
      .field('email', email)
      .field('password', password)
      .attach('image', filePath);

    expect(response.status).toBe(201);
  });

  it('should return a 200 OK response and log in an admin user with valid email and password', async () => {
    const response = await supertest(app).post('/api/auth/login').send({ email, password });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('token', expect.any(String));

    expect(response.body).toHaveProperty('user');
    const { user } = response.body;
    userId = user.id;

    bearerToken = `Bearer ${response.body.token}`;

    expect(bearerToken).toBeDefined();

    expect(user).toHaveProperty('id', expect.any(Number));
    expect(user).toHaveProperty('email', email);
    expect(user).toHaveProperty('role', 'user');
  });

  it(`should return a 202 response for deleting user: ${email}`, async () => {
    const response = await supertest(app)
      .delete(`/api/user/${userId}`)
      .set('Authorization', bearerToken);

    expect(response.status).toBe(202);
  });

  it('should handle invalid email and password', async () => {
    const invalidEmail = 'invalid@example.com';
    const invalidPassword = 'invalidpassword';

    const response = await supertest(app)
      .post('/api/auth/login')
      .send({ email: invalidEmail, password: invalidPassword });

    expect(response.status).toBe(505);
  });
});
