/** @format */

import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
// import { Models } from '../interfaces/general';

interface ExperienceAttributes {
  id: number;
  userId: number;
  companyName: string;
  role: string;
  startDate: string;
  endDate: string;
}

export class Experience
  extends Model<ExperienceAttributes, Optional<ExperienceAttributes, 'id'>>
  implements ExperienceAttributes
{
  id: number;

  userId: number;

  companyName: string;

  role: string;

  startDate: string;

  endDate: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    Experience.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          type: DataTypes.INTEGER.UNSIGNED,
          field: 'user_id',
          allowNull: false,
          references: {
            model: 'users',
            key: 'id',
          },
        },
        companyName: {
          type: DataTypes.STRING(256),
          field: 'company_name',
          allowNull: false,
        },
        role: {
          type: DataTypes.STRING(256),
          allowNull: false,
        },
        startDate: {
          type: DataTypes.DATE,
          field: 'startDate',
        },
        endDate: {
          type: DataTypes.DATE,
          field: 'endDate',
        },
      },
      {
        tableName: 'experiences',
        underscored: true,
        sequelize,
        // createdAt: 'created_at',
        // updatedAt: 'updated_at',
        timestamps: true,
      },
    );
  }

  static associate() {}
}
