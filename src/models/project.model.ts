/** @format */

import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';

interface ProjectAttributes {
  id: number;
  userId: number;
  image: string;
  description: string;
}

export class Project
  extends Model<ProjectAttributes, Optional<ProjectAttributes, 'id'>>
  implements ProjectAttributes
{
  id: number;

  userId: number;

  image: string;

  description: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    Project.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false,
          field: 'user_id',
          references: {
            model: 'users',
            key: 'id',
          },
        },
        image: {
          type: DataTypes.STRING(256),
          allowNull: false,
        },
        description: {
          type: DataTypes.STRING(256),
          allowNull: false,
        },
      },
      {
        tableName: 'projects',
        underscored: true,
        sequelize,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        timestamps: true,
      },
    );
  }

  static associate(models: Models) {
    Project.belongsTo(models.user, { foreignKey: 'user_id' });
  }
}
